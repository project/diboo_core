# Diboo Core

## Add new field to the node types of diboo

First define a new field in the `\Drupal\diboo_core\Entity` namespace.
Follow the other examples in that same namespace with the `Field` suffix.

Install the storage for that field:
```php
use Drupal\diboo_core\Entity\MaxOpenChainsField;

function diboo_core_update_10100() {
  $storage_definition = MaxOpenChainsField::getStorageDefinition('node');

  $update_manager = \Drupal::entityDefinitionUpdateManager();
  $update_manager->installFieldStorageDefinition(MaxOpenChainsField::FIELD_NAME, 'node', 'diboo_core', $storage_definition);
}
```
