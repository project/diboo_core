# Diboo core

Base functionality for a diboo installation.

Diboo is like a game of telephone, or [Chinese whispers](https://en.wikipedia.org/wiki/Chinese_whispers)
but using written sentences and drawings.

## Rules

Typically a chain is started with a phrase. The next contributor
of the chain will see the initial phrase and will have to describe it with a
drawing. Then another contributor adding will only see the previous
drawing and will describe what they see in the image, adding that contribution to the chain .
The cycle is repeated many times

When a chain is finished all the chain links will be published and can be viewed
in chronological order for everyone's joy.

A chain can be automatically published when it reaches a number of chain links.

## Setup

There is three basic functions a node type can act as:

* Room: Group of chains, open to participation or closed (public). Holds the rules for each chain.
* Chain: Set of chain links.
* Chain link: A phrase or image contribution to a chain.

At least the following is required:

* One room content type
* One chain content type
* Two chain link types of a different chain link each
* A drawing tool, for example: [Signature pad](https://www.drupal.org/project/diboo_signature_pad)

The module [Diboo kickstart](https://www.drupal.org/project/diboo_kickstart) will prepare basic setup when installed.

## Features

### Chain locking

When a user starts drawing or describing a drawing that chain will be
unavailable for other contributors until they are finished.

A time limit can be set at the room so chains are unlocked automatically
when certain minutes have passed. Cron needs to run at least as often as
the number of minutes configured for a room, 100 minutes by default.

### Open chains limits

Each room can have room limits for how many chains can be open at the same time.
A per-user limit can also be set.

## Roadmap

* Ability to have rooms for groups of users
* Different drawing tools
* More chain link types, for example, emojis.
* Adding chain links as anonymous.
* Publish chains automatically on a dates or after some period of being opened.
* Comments/reactions on chains or chain links
* Notifications to all participants of a chain when is published.
* Moderation: ability to revert drawings or descriptions that derail the chain on purpose.

## Similar apps

### [Doodle or die](https://doodleordie.com)

Non-free software, English. Many UI differences, e.g. Prompts users to play on the next chain immediately.
Big user base.

### [Drawception](https://drawception.com)

Non-free software, English. Features can be unlocked by paying like new color palettes.
Big user base.

### [Eat poop you cat](https://github.com/JamesOsborn-SE/eat-poop-you-cat-android) Android

Offline game that works by passing the phone to the next participant.
Free software. Community translations.
Get it on [F-Droid](https://f-droid.org/packages/dev.develsinthedetails.eatpoopyoucat/)

## Differences with similar apps

The aim of diboo is to be a game with input and collaboration from the community that is released
as free software and therefore can be self-hosted.

## Offline playing

Gather with your friends, grab a piece of paper and some pens and follow the same rules by folding the paper.
[Examples](https://boardgamegeek.com/boardgame/30618/eat-poop-you-cat)
