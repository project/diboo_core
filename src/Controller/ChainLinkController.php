<?php

namespace Drupal\diboo_core\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\diboo_core\Entity\Chain;
use Drupal\node\Entity\NodeType;
use Drupal\node\NodeTypeInterface;

/**
 * Returns responses for chain link routes.
 */
class ChainLinkController extends ControllerBase {

  /**
   * The _title_callback for diboo_core.add_chain_link route.
   *
   * @param \Drupal\node\NodeTypeInterface $node_type
   *   The current node type.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The page title.
   */
  public function addChainLinkPageTitle(NodeTypeInterface $node_type): TranslatableMarkup {
    if ($node_type->id() === 'diboo_image') {
      return $this->t('Draw',);
    }
    else {
      return $this->t('Describe the picture',);
    }
  }

  /**
   * Access callback for adding a link to a chain.
   *
   * This check can not live in the Chain class because it
   * need the node_type route parameter.
   *
   * @param \Drupal\diboo_core\Entity\Chain $chain
   *   Chain.
   * @param \Drupal\node\Entity\NodeType $node_type
   *   Node type that will be the chain link.
   */
  public function addChainLinkAccess(Chain $chain, NodeType $node_type): AccessResultInterface {

    // The node type must be a chain link.
    // @todo Generic way of retrieving chain link types.
    if ($node_type->id() !== 'diboo_phrase' && $node_type->id() !== 'diboo_image') {
      return AccessResult::forbidden()->addCacheableDependency($node_type);
    }

    $chainLinks = $chain->getChainLinks()->referencedEntities();

    // The last chain link must not be of the same type.
    $lastChainLink = end($chainLinks);
    if (!$lastChainLink || $lastChainLink->bundle() === $node_type->id()) {
      return AccessResult::forbidden()
        ->addCacheableDependency($node_type)
        ->addCacheableDependency($chain);
    }
    return AccessResult::allowed()->addCacheableDependency($chain);
  }

}
