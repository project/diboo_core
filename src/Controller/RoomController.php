<?php

namespace Drupal\diboo_core\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\diboo_core\Entity\Room;
use Drupal\node\Entity\NodeType;
use Drupal\node\NodeInterface;
use Drupal\node\NodeTypeInterface;

/**
 * Returns responses for Diboo core routes.
 */
class RoomController extends ControllerBase {

  /**
   * The _title_callback for the route to start a chain.
   */
  public function newChainPageTitle(NodeInterface $room, NodeTypeInterface $node_type) : TranslatableMarkup {
    return $this->t('@room: start a chain with a @chain_link_type', [
      '@room' => $room->label(),
      '@chain_link_type' => $node_type->label(),
    ]);
  }

  /**
   * Access callback for starting a chain.
   *
   * This check can not live in the Chain class because it
   * need the node_type route parameter.
   *
   * @param \Drupal\diboo_core\Entity\Room $room
   *   Room.
   * @param \Drupal\node\Entity\NodeType $node_type
   *   Node type that will be the first chain link.
   */
  public function startChainAccess(Room $room, NodeType $node_type): AccessResultInterface {
    $allowedLinkTypes = array_column($room->getAllowedFirstLinkTypes()->getValue(), 'target_id');
    if (!in_array($node_type->id(), $allowedLinkTypes, TRUE)) {
      return AccessResult::forbidden()
        ->addCacheableDependency($room);
    }
    return AccessResult::allowed()->addCacheableDependency($room);
  }

}
