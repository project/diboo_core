<?php

namespace Drupal\diboo_core\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\node\Entity\Node;

/**
 * Bundle class.
 */
class Chain extends Node {

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions): array {
    $definitions = parent::bundleFieldDefinitions($entity_type, $bundle, $base_field_definitions);

    $definitions[ChainLinksField::FIELD_NAME] = ChainLinksField::getFieldDefinition($entity_type->id(), $bundle);
    $definitions[RoomsField::FIELD_NAME] = RoomsField::getFieldDefinition($entity_type->id(), $bundle);
    $definitions[CurrentContributorsField::FIELD_NAME] = CurrentContributorsField::getFieldDefinition($entity_type->id(), $bundle);
    $definitions[FinishedField::FIELD_NAME] = FinishedField::getFieldDefinition($entity_type->id(), $bundle);
    return $definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation = 'view', ?AccountInterface $account = NULL, $return_as_object = FALSE): bool|AccessResultInterface {
    if ($operation !== 'add_chain_link') {
      return parent::access($operation, $account, $return_as_object);
    }
    // A published chain is closed for adding more links.
    if ($this->isPublished()) {
      $result = AccessResult::forbidden();
      return $return_as_object ? $result : $result->isAllowed();
    }

    if ($this->bundle() !== 'diboo_chain') {
      $result = AccessResult::forbidden();
      return $return_as_object ? $result : $result->isAllowed();
    }

    // Deny access when the chain is locked
    // and the current user is not locking it.
    if (
      !$this->get('diboo_current_contributors')->isEmpty() &&
      array_search(
        needle: ['target_id' => $account->id()],
        haystack: $this->get('diboo_current_contributors')->getValue()
      ) === FALSE
    ) {
      $result = AccessResult::forbidden()->addCacheableDependency($this)->addCacheableDependency($account);
      return $return_as_object ? $result : $result->isAllowed();
    }

    // The latest chain links should be from other contributors.
    $chainLinkNodes = $this->getChainLinkNodes();
    $rooms = $this->getRoomNodes();
    $room = reset($rooms);
    $minBetweenChainLinks = intval($room->get('diboo_min_chain_links_between')->getString());
    for ($i = 0; $i < $minBetweenChainLinks; $i++) {
      $chainLink = array_pop($chainLinkNodes);
      if (!$chainLink) {
        break;
      }
      if ($chainLink->getOwnerId() === $account->id()) {
        $result = AccessResult::forbidden()->addCacheableDependency($this)->addCacheableDependency($account);
        return $return_as_object ? $result : $result->isAllowed();
      }
    }

    $result = AccessResult::allowed();
    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    // Set the room from the route.
    if ($this->getRooms()->isEmpty() && $room = \Drupal::routeMatch()->getParameter('room')) {
      $this->assignRoom($room);
    }
    /** @var \Drupal\diboo_core\Entity\Chain $original */
    $original = $this->original;
    // When the chain is published, publish all chain links.
    if ($this->isPublished() && !$original->isPublished()) {
      $chainLinks = $this->getChainLinkNodes();

      // Fill title of images with the previous images.
      $lastPhrase = '';
      foreach ($chainLinks as $chainLink) {
        if ($chainLink->bundle() === 'diboo_phrase') {
          $lastPhrase = $chainLink->label();
        }
        if ($chainLink->bundle() === 'diboo_image' && $lastPhrase) {
          $chainLink->set('title', $lastPhrase);
          // @todo Modify output of alt to display "A drawing of ALT"
          // translated to current language.
          $chainLink->get('diboo_image')->first()->set('alt', $lastPhrase);
        }
        $chainLink->setPublished();
        $chainLink->save();
      }

      // When there is a first and last phrase
      // use them to give the chain a title.
      if (
        $lastPhrase &&
        ($firstPhrase = reset($chainLinks)) &&
        $firstPhrase->bundle() === 'diboo_phrase'
      ) {
        $chainTitle = (string) (new TranslatableMarkup(
          'From "@init" to "@last"',
          [
            // Use Markup to avoid escaping of characters like `"`.
            '@init' => Markup::create($firstPhrase->label()),
            '@last' => Markup::create($lastPhrase),
          ],
          ['langcode' => $this->get('langcode')->getString()],
        ))->render();
        $this->set('title', $chainTitle);
      }
    }
  }

  /**
   * Arrange diboo fields for the full view mode.
   */
  public static function configureViewModeFull(EntityViewDisplayInterface $display): void {
    $display->setComponent('diboo_chain_links', [
      'weight' => 0,
      'type' => 'entity_reference_entity_view',
      'label' => 'hidden',
      'settings' => [
        'view_mode' => 'diboo_chain_link',
        'link' => FALSE,
      ],
      'third_party_settings' => [],
    ]);
    $display->removeComponent('diboo_rooms');
  }

  /**
   * When chains are created for the first time they need to get a room.
   */
  protected function assignRoom(Room $room): void {
    // Chains start hidden and will be published once
    // they receive some chain links.
    $this->setUnpublished();
    $this->set('diboo_rooms', [
      'target_id' => $room->id(),
    ]);
    // Chains are containers for chain links so they have no title
    // when they are created.
    $this->set('title', $this->label() ?: $this->uuid());
  }

  /**
   * Get chain links.
   *
   * @return \Drupal\Core\Field\EntityReferenceFieldItemListInterface
   *   Field data.
   */
  public function getChainLinks(): EntityReferenceFieldItemListInterface {
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
    $items = $this->get('diboo_chain_links');
    return $items;
  }

  /**
   * Get chain link nodes.
   *
   * @return \Drupal\diboo_core\Entity\ChainLink[]
   *   Chain links.
   */
  public function getChainLinkNodes(): array {
    /** @var \Drupal\diboo_core\Entity\ChainLink[] */
    $nodes = $this->getChainLinks()->referencedEntities();
    return $nodes;
  }

  /**
   * Get rooms.
   *
   * @return \Drupal\Core\Field\EntityReferenceFieldItemListInterface
   *   Field data.
   */
  public function getRooms(): EntityReferenceFieldItemListInterface {
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
    $items = $this->get('diboo_rooms');
    return $items;
  }

  /**
   * Get room nodes.
   *
   * @return \Drupal\diboo_core\Entity\Room[]
   *   Rooms.
   */
  public function getRoomNodes(): array {
    /** @var \Drupal\diboo_core\Entity\Room[] */
    $nodes = $this->getRooms()->referencedEntities();
    return $nodes;
  }

  /**
   * Get the main room, the first referenced.
   */
  public function getMainRoomNode(): ?Room {
    $rooms = $this->getRoomNodes();
    $room = reset($rooms);
    return $room ?: NULL;
  }

  /**
   * Get the the user that is currently adding a chain link.
   */
  public function getCurrentContributorId(): ?int {
    if ($this->get('diboo_current_contributors')->isEmpty()) {
      return NULL;
    }
    $contributors = array_column($this->get('diboo_current_contributors')->getValue(), 'target_id');
    return reset($contributors);
  }

  /**
   * Lock the chain.
   *
   * @param int $id
   *   User ID.
   *
   * @return bool
   *   Wether the chain was actually locked.
   */
  public function lockForUserId($id): bool {
    if ($this->get('diboo_current_contributors')->isEmpty()) {
      $this->get('diboo_current_contributors')->appendItem([
        'target_id' => $id,
      ]);
      $this->save();
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Unlock the chain.
   */
  public function unlock(): self {
    $this->set('diboo_current_contributors', []);
    $this->save();
    return $this;
  }

}
