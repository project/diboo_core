<?php

namespace Drupal\diboo_core\Entity;

use Drupal\Core\Config\Entity\ThirdPartySettingsInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Bundle class.
 */
class ChainLink extends Node {

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions): array {
    $definitions = parent::bundleFieldDefinitions($entity_type, $bundle, $base_field_definitions);
    $nodeTypeStorage = \Drupal::entityTypeManager()->getStorage('node_type');
    $nodeType = $nodeTypeStorage->load($bundle);
    if (!$nodeType || !$nodeType instanceof ThirdPartySettingsInterface) {
      return $definitions;
    }
    // Only images need an extra field.
    if ($bundle === 'diboo_image') {
      $definitions[ImageField::FIELD_NAME] = ImageField::getFieldDefinition($entity_type->id(), $bundle);
    }

    return $definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);

    if ($this->isNew()) {
      $this->set('status', NodeInterface::NOT_PUBLISHED);
      $this->set('promote', NodeInterface::NOT_PROMOTED);
    }

    // Chain links can only be created in the context of a chain.
    if (!\Drupal::routeMatch()->getParameter('chain')) {
      return;
    }
    // Chains links that don't contain a phrase are containers for
    // other content such as images so they have no title
    // when they are created.
    if ($this->get('title')->isEmpty()) {
      $this->set('title', $this->uuid());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE): void {
    parent::postSave($storage, $update);
    if ($update) {
      return;
    }

    $chain = \Drupal::routeMatch()->getParameter('chain');
    // New chain.
    if (!$chain) {
      $chain = $storage->create([
        'type' => 'diboo_chain',
        'status' => $this::NOT_PUBLISHED,
      ]);
    }

    /** @var \Drupal\diboo_core\Entity\Chain $chain **/
    $chain->getChainLinks()->appendItem([
      'target_id' => $this->id(),
    ]);

    if (
      $chain->getMainRoomNode() &&
      count($chain->getChainLinks()) >= $chain->getMainRoomNode()->getMinChainLinksToPublish()
    ) {
      $chain->setPublished();
      $chain->setPromoted(FALSE);
      $chain->set('diboo_finished', \Drupal::time()->getRequestTime());
    }

    $chain->unlock();
  }

  /**
   * Get the type of chain link given by its bundle.
   *
   * In the diboo core module this is one of "phrase" or "image"
   * phrase being the empty value.
   */
  public function getChainLinkType(): string {
    return $this->get('type')->first()->get('entity')->getValue()->getThirdPartySetting('diboo_core', 'chain_link_type') ?: 'phrase';
  }

}
