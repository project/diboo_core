<?php

namespace Drupal\diboo_core\Entity;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides storage and field definitions.
 */
final class CurrentContributorsField {

  /**
   * The field name.
   */
  public const FIELD_NAME = 'diboo_current_contributors';

  /**
   * Get the field storage definition.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return \Drupal\entity\BundleFieldDefinition
   *   The storage definition.
   */
  public static function getStorageDefinition(string $entity_type_id): BundleFieldDefinition {
    return self::bundleFieldDefinition()
      ->setTargetEntityTypeId($entity_type_id);
  }

  /**
   * Get the field definition.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle
   *   The entity bundle.
   *
   * @return \Drupal\entity\BundleFieldDefinition
   *   The field definition.
   */
  public static function getFieldDefinition(string $entity_type_id, string $bundle): BundleFieldDefinition {
    return self::bundleFieldDefinition()
      ->setTargetEntityTypeId($entity_type_id)
      ->setTargetBundle($bundle)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_label',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -5,
      ]);
  }

  /**
   * Get the bundle field definition.
   *
   * @return \Drupal\entity\BundleFieldDefinition
   *   The bundle field definition.
   */
  private static function bundleFieldDefinition(): BundleFieldDefinition {
    return BundleFieldDefinition::create('entity_reference')
      ->setProvider('diboo_core')
      ->setName(self::FIELD_NAME)
      ->setLabel(new TranslatableMarkup('Current contributors'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSettings([
        'target_type' => 'user',
        'handler' => 'default:user',
        'handler_settings' => [
          'target_bundles' => NULL,
          'auto_create' => FALSE,
          'include_anonymous' => TRUE,
          'sort' => [
            'field' => '_none',
            'direction' => 'ASC',
          ],
          'filter' => [
            'type' => '_none',
          ],
        ],
      ]);
  }

}
