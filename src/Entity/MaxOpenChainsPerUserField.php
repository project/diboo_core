<?php

namespace Drupal\diboo_core\Entity;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides storage and field definitions.
 */
final class MaxOpenChainsPerUserField {

  /**
   * The field name.
   */
  public const FIELD_NAME = 'diboo_max_open_chains_user';

  /**
   * Get the field storage definition.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return \Drupal\entity\BundleFieldDefinition
   *   The storage definition.
   */
  public static function getStorageDefinition(string $entity_type_id): BundleFieldDefinition {
    return self::bundleFieldDefinition()
      ->setTargetEntityTypeId($entity_type_id);
  }

  /**
   * Get the field definition.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle
   *   The entity bundle.
   *
   * @return \Drupal\entity\BundleFieldDefinition
   *   The field definition.
   */
  public static function getFieldDefinition(string $entity_type_id, string $bundle): BundleFieldDefinition {
    return self::bundleFieldDefinition()
      ->setTargetEntityTypeId($entity_type_id)
      ->setTargetBundle($bundle)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE);
  }

  /**
   * Get the bundle field definition.
   *
   * @return \Drupal\entity\BundleFieldDefinition
   *   The bundle field definition.
   */
  private static function bundleFieldDefinition(): BundleFieldDefinition {
    return BundleFieldDefinition::create('integer')
      ->setProvider('diboo_core')
      ->setName(self::FIELD_NAME)
      ->setLabel(new TranslatableMarkup('Maximum open chains per user'))
      ->setDescription(new TranslatableMarkup('Zero for no limit. Setting a limit per user can prevent a user to fill the room with open chains.'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setDefaultValue(['value' => \Drupal::config('diboo_core.settings')->get('max_open_chains_user')])
      ->setSettings([
        // Storage settings.
        'unsigned' => FALSE,
        'size' => 'normal',
        // Field settings.
        'min' => NULL,
        'max' => NULL,
        'prefix' => '',
        'suffix' => '',
      ]);
  }

}
