<?php

namespace Drupal\diboo_core\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\node\Entity\Node;

/**
 * Bundle class.
 */
class Room extends Node {

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions): array {
    $definitions = parent::bundleFieldDefinitions($entity_type, $bundle, $base_field_definitions);

    $definitions[AllowedFirstLinkTypesField::FIELD_NAME] = AllowedFirstLinkTypesField::getFieldDefinition($entity_type->id(), $bundle);
    $definitions[MaxMinutesChainLockedField::FIELD_NAME] = MaxMinutesChainLockedField::getFieldDefinition($entity_type->id(), $bundle);
    $definitions[MaxOpenChainsField::FIELD_NAME] = MaxOpenChainsField::getFieldDefinition($entity_type->id(), $bundle);
    $definitions[MaxOpenChainsPerUserField::FIELD_NAME] = MaxOpenChainsPerUserField::getFieldDefinition($entity_type->id(), $bundle);
    $definitions[MinChainLinksBetweenContributionsField::FIELD_NAME] = MinChainLinksBetweenContributionsField::getFieldDefinition($entity_type->id(), $bundle);
    $definitions[MinChainLinksToPublishField::FIELD_NAME] = MinChainLinksToPublishField::getFieldDefinition($entity_type->id(), $bundle);

    return $definitions;
  }

  /**
   * Get Allowed first link types.
   *
   * @return \Drupal\Core\Field\EntityReferenceFieldItemListInterface
   *   Field data.
   */
  public function getAllowedFirstLinkTypes(): EntityReferenceFieldItemListInterface {
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
    $items = $this->get('diboo_allowed_first_link_types');
    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation = 'view', ?AccountInterface $account = NULL, $return_as_object = FALSE): bool|AccessResultInterface {
    if ($operation !== 'new_chain') {
      return parent::access($operation, $account, $return_as_object);
    }

    if (!$this->isPublished()) {
      $result = AccessResult::forbidden()->addCacheableDependency($this);
      return $return_as_object ? $result : $result->isAllowed();
    }
    $maxOpenChains = intval($this->get('diboo_max_open_chains')->getString());

    if ($this->getOpenChainsCount() >= $maxOpenChains) {
      $result = AccessResult::forbidden()
        ->addCacheableDependency($this)
        ->addCacheTags(['node_list']);
      return $return_as_object ? $result : $result->isAllowed();
    }

    $maxOpenChainsPerUser = intval($this->get('diboo_max_open_chains_user')->getString());

    if ($this->getOpenChainsCount($account->id()) >= $maxOpenChainsPerUser) {
      $result = AccessResult::forbidden()
        ->addCacheableDependency($this)
        ->addCacheTags(['node_list']);
      return $return_as_object ? $result : $result->isAllowed();
    }

    $result = AccessResult::allowed();
    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * Gets the number of open chains (not published).
   *
   * @param int|string|null $userId
   *   Filter by chains that the user started.
   *
   * @return int
   *   Count.
   */
  public function getOpenChainsCount($userId = NULL): int {
    return count($this->getOpenChainsResult($userId));
  }

  /**
   * Arrange diboo fields for the full view mode.
   */
  public static function configureViewModeFull(EntityViewDisplayInterface $display): void {
    $display->setComponent('diboo_allowed_first_link_types', [
      'weight' => 0,
      'type' => 'entity_link',
      'label' => 'hidden',
      'settings' => [
        'link_template' => 'node:diboo_start_chain',
        'link_text' => new TranslatableMarkup('Start a new chain with a [content-type:name]'),
        'route_parameter_first' => '[node:nid]',
        'route_parameter_context_first' => 'displayed_entity',
        'route_parameter_second' => '[content-type:machine-name]',
        'route_parameter_context_second' => 'referenced_entity',
        'route_parameter_third' => '',
        'route_parameter_context_third' => 'referenced_entity',
        'destination' => '[current-destination]',
        'destination_context' => 'displayed_entity',
      ],
    ]);
  }

  /**
   * Get configuration for publishing a chain after a number of links.
   */
  public function getMinChainLinksToPublish(): int {
    return intval($this->get('diboo_min_chain_links_publish')->getString());
  }

  /**
   * Gets a list of open chains IDs (not published).
   *
   * @param int|string|null $userId
   *   Filter by chains that the user started.
   *
   * @return array
   *   Node IDs (in ascending order).
   */
  protected function getOpenChainsResult($userId = NULL): array {
    $query = \Drupal::entityTypeManager()->getStorage('node')->getQuery()
      ->accessCheck(FALSE)
      ->condition('diboo_rooms', $this->id())
      ->condition('status', $this::NOT_PUBLISHED)
      // Chains with the same language as the room.
      ->condition('langcode', $this->get('langcode')->getString())
      ->sort('created', 'ASC');
    if (!is_null($userId)) {
      $query->condition('uid', $userId);
    }
    return $query->execute();
  }

  /**
   * Gets a list of open chains entities (not published).
   *
   * @return array
   *   Node IDs (in ascending order).
   */
  public function getOpenChains(): array {
    return \Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadMultiple($this->getOpenChainsResult());
  }

}
