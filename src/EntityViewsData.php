<?php

namespace Drupal\diboo_core;

use Drupal\entity\BundleFieldDefinition;
use Drupal\entity\EntityViewsData as EntityApiViewsData;

/**
 * Provides improvements to core's generic views integration for entities.
 */
class EntityViewsData extends EntityApiViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $this->tableMapping = $this->storage->getTableMapping();
    $entity_type_id = $this->entityType->id();

    $data = [];

    // Copy from entity module but without the bundle plugin requirement.
    $bundles = $this->getEntityTypeBundleInfo()->getBundleInfo($entity_type_id);
    foreach (array_keys($bundles) as $bundle) {
      $field_definitions = $this->getEntityFieldManager()->getFieldDefinitions($entity_type_id, $bundle);
      foreach ($field_definitions as $field_definition) {
        if ($field_definition instanceof BundleFieldDefinition) {
          $this->addBundleFieldData($data, $field_definition);
        }
      }
    }

    return $data;
  }

}
