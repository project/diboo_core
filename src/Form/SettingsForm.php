<?php

namespace Drupal\diboo_core\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure settings for this module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'diboo_core_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['diboo_core.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['intro'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Define here the defaults when a new room is created. Then each chain belonging to that room will follow the rules defined in the room'),
    ];
    $settings = $this->config('diboo_core.settings');
    $form['max_open_chains'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum number of open chains in a room'),
      '#description' => $this->t('Leave it empty for no limit'),
      '#default_value' => $settings->get('max_open_chains'),
    ];
    $form['max_open_chains_user'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum number of open chains in a room by the same user'),
      '#description' => $this->t('Leave it empty for no limit'),
      '#default_value' => $settings->get('max_open_chains_user'),
    ];
    $form['min_chain_links_between'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of chain links that need to be added before the same user can add again in the same chain.'),
      '#description' => $this->t('Set it to a high number like 5 to avoid repeated people participating on the same chain'),
      '#required' => TRUE,
      '#default_value' => $settings->get('min_chain_links_between'),
    ];
    $form['min_chain_links_publish'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of chain links required to trigger the publication of the chain.'),
      '#description' => $this->t('When the number is reached the chain will be closed for more contributions and everyone can see it.'),
      '#required' => TRUE,
      '#default_value' => $settings->get('min_chain_links_publish'),
    ];
    $form['max_minutes_chain_lock'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum minutes for the chain to be locked'),
      '#description' => $this->t('Requires cron to be configured'),
      '#required' => TRUE,
      '#default_value' => $settings->get('max_minutes_chain_lock'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $maxOpenChains = $form_state->getValue('max_open_chains');
    if ($maxOpenChains && !is_numeric($maxOpenChains)) {
      $form_state->setErrorByName('max_open_chains', $this->t('The value should be a number.'));
    }

    $chainLinksBetween = $form_state->getValue('min_chain_links_between');
    if (!is_numeric($chainLinksBetween) || intval($chainLinksBetween) < 0) {
      $form_state->setErrorByName('min_chain_links_between', $this->t('The value should be a number bigger than 0.'));
    }

    $chainLinksRequired = $form_state->getValue('min_chain_links_publish');
    if (!is_numeric($chainLinksRequired) || intval($chainLinksRequired) < 2) {
      $form_state->setErrorByName('min_chain_links_publish', $this->t('The value should be a number bigger than 2.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('diboo_core.settings')
      ->set('max_open_chains', $form_state->getValue('max_open_chains'))
      ->set('max_open_chains_user', $form_state->getValue('max_open_chains_user'))
      ->set('min_chain_links_between', $form_state->getValue('min_chain_links_between'))
      ->set('min_chain_links_publish', $form_state->getValue('min_chain_links_publish'))
      ->set('max_minutes_chain_lock', $form_state->getValue('max_minutes_chain_lock'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
