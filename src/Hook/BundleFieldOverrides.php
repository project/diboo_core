<?php

namespace Drupal\diboo_core\Hook;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\Entity\BaseFieldOverride;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Handle field overrides for diboo fields.
 */
#[Hook('entity_bundle_field_info')]
class BundleFieldOverrides {

  use StringTranslationTrait;

  /**
   * Implements hook_entity_bundle_field_info().
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   *   The entity type definition.
   * @param string $bundle
   *   The bundle.
   * @param \Drupal\Core\Field\FieldDefinitionInterface[] $baseFieldDefinitions
   *   The list of base field definitions for the entity type.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of bundle field definitions, keyed by field name.
   */
  public function __invoke(EntityTypeInterface $entityType, $bundle, array $baseFieldDefinitions): array {
    $fields = [];
    if ($entityType->id() !== 'node' || $bundle !== 'diboo_phrase' || empty($baseFieldDefinitions['title'])) {
      return $fields;
    }
    $field = BaseFieldOverride::createFromBaseFieldDefinition($baseFieldDefinitions['title'], $bundle);
    $field->setLabel($this->t('Phrase'));
    $fields['title'] = $field;
    return $fields;
  }

}
