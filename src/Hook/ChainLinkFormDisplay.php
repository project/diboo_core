<?php

namespace Drupal\diboo_core\Hook;

use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Hook for configuring the form mode when adding a chain link.
 */
class ChainLinkFormDisplay {

  use StringTranslationTrait;

  /**
   * Constructs a ChainLinkFormDisplay object.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route matcher.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   */
  public function __construct(
    private RouteMatchInterface $routeMatch,
    private AccountProxyInterface $currentUser,
  ) {
  }

  /**
   * Implements hook_entity_form_display_alter().
   *
   * Configure the form display.
   *
   * @param \Drupal\Core\Entity\Display\EntityFormDisplayInterface $form_display
   *   The object that is used to display the form components.
   * @param array $context
   *   An associative array containing:
   *     entity_type: The entity type, e.g., 'node' or 'user'.
   *     bundle: The bundle, e.g., 'page' or 'article'.
   *     form_mode: The form mode; e.g., 'default', 'profile', 'register', etc.
   */
  #[Hook('entity_form_display_alter')]
  public function entityFormDisplayAlter(EntityFormDisplayInterface $form_display, array $context): void {
    if ($context['form_mode'] !== 'diboo_chain_link') {
      return;
    }
    // Hide all components except the main widget for this
    // chain link and the extra field that displays the last chain link.
    $isImage = $form_display->getComponent('diboo_image');
    $componentToDisplay = $isImage ? 'diboo_image' : 'title';
    foreach (array_keys($form_display->getComponents()) as $componentName) {
      if ($componentName === $componentToDisplay) {
        continue;
      }
      $form_display->removeComponent($componentName);
    }
    $chainStarting = $this->routeMatch->getRouteName() === 'diboo_core.start_chain';
    // Change label for the phrase field (title of the node).
    if (!$isImage && !$chainStarting && ($title = $form_display->getComponent('title'))) {
      $title['third_party_settings']['change_labels'] = [
        'field_label_overwrite' => $this->t('Describe the picture'),
      ];
      $form_display->setComponent('title', $title);
    }
    if ($chainStarting) {
      $form_display->setThirdPartySetting('change_labels', 'submit_label', $this->t('Start a new chain'));
      $form_display->setThirdPartySetting('change_labels', 'submit_message', $this->t('New chain started'));
    }
    else {
      $type = $isImage ? $this->t('picture') : $this->t('phrase');
      $form_display->setThirdPartySetting('change_labels', 'submit_label', $this->t('Add @type to the chain', ['@type' => $type]));
      $form_display->setThirdPartySetting('change_labels', 'submit_message', $this->t('New chain link added'));
    }
    // When there is a chain in the route lock it for this user.
    if ($chain = $this->routeMatch->getParameter('chain')) {
      $chain->lockForUserId($this->currentUser->id());
    }
  }

}
