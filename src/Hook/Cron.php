<?php

namespace Drupal\diboo_core\Hook;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\State\StateInterface;

/**
 * Handle cron tasks.
 */
class Cron {

  /**
   * Constructs a Cron object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected TimeInterface $time,
    protected StateInterface $state,
  ) {
  }

  /**
   * Implements hook_cron().
   */
  #[Hook('cron')]
  public function cron(): void {
    $storage = $this->entityTypeManager->getStorage('node');
    $lockedChains = $storage->loadMultiple($storage->getQuery()->accessCheck(FALSE)->exists('diboo_current_contributors')->execute());
    /** @var \Drupal\diboo_core\Entity\Chain $lockedChain */
    foreach ($lockedChains as $lockedChain) {
      $room = $lockedChain->getMainRoomNode();
      // Locking a chain updates the "changed" field.
      // The delta between "now" and the last time the chain changed is
      // bigger than the limit of minutes it can stay locked.
      if ($this->time->getCurrentTime() - $lockedChain->getChangedTime() > (int) $room->get('diboo_max_minutes_chain_lock')->getString() * 60) {
        // Keep a record of who locked a chain.
        $lockingUserId = $lockedChain->getCurrentContributorId();
        $unlocks = $this->state->get('diboo_core_unlocks', []);
        $unlocks[$lockingUserId] = 1 + ($unlocks[$lockingUserId] ?? 0);
        $this->state->set('diboo_core_unlocks', $unlocks);
        $lockedChain->unlock();
      }
    }
  }

}
