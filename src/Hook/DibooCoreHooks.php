<?php

namespace Drupal\diboo_core\Hook;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\diboo_core\Entity\AllowedFirstLinkTypesField;
use Drupal\diboo_core\Entity\Chain;
use Drupal\diboo_core\Entity\ChainLinksField;
use Drupal\diboo_core\Entity\CurrentContributorsField;
use Drupal\diboo_core\Entity\FinishedField;
use Drupal\diboo_core\Entity\Image;
use Drupal\diboo_core\Entity\ImageField;
use Drupal\diboo_core\Entity\MaxMinutesChainLockedField;
use Drupal\diboo_core\Entity\MaxOpenChainsField;
use Drupal\diboo_core\Entity\MaxOpenChainsPerUserField;
use Drupal\diboo_core\Entity\MinChainLinksBetweenContributionsField;
use Drupal\diboo_core\Entity\MinChainLinksToPublishField;
use Drupal\diboo_core\Entity\Phrase;
use Drupal\diboo_core\Entity\Room;
use Drupal\diboo_core\Entity\RoomsField;

/**
 * Hook implementations for diboo_core.
 */
class DibooCoreHooks {

  /**
   * Implements hook_theme().
   *
   * Use module-provided templates for diboo content types.
   *
   * @return array
   *   Theme definitions.
   */
  #[Hook('theme')]
  public function theme(): array {
    return [
      'node__diboo_open_chain' => [
        'base hook' => 'node',
      ],
      'node__diboo_chain_link' => [
        'base hook' => 'node',
      ],
    ];
  }

  /**
   * Implements hook_entity_type_alter().
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface[] $entity_types
   *   An associative array of all entity type definitions.
   */
  #[Hook('entity_type_alter')]
  public function entityTypeAlter(array &$entity_types): void {
    $entity_types['node']
      ->setFormClass('diboo_chain_link', 'Drupal\node\NodeForm')
      ->setLinkTemplate('diboo_start_chain', '/node/{room}/diboo-start-chain/{node_type}');
  }

  /**
   * Implements hook_entity_field_storage_info().
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   Entity type.
   *
   * @return array
   *   Field definitions.
   */
  #[Hook('entity_field_storage_info')]
  public function entityFieldStorageInfo(EntityTypeInterface $entity_type): array {
    $definitions = [];
    if ($entity_type->id() !== 'node') {
      return $definitions;
    }
    $definitions[AllowedFirstLinkTypesField::FIELD_NAME] = AllowedFirstLinkTypesField::getStorageDefinition($entity_type->id());
    $definitions[ChainLinksField::FIELD_NAME] = ChainLinksField::getStorageDefinition($entity_type->id());
    $definitions[CurrentContributorsField::FIELD_NAME] = CurrentContributorsField::getStorageDefinition($entity_type->id());
    $definitions[FinishedField::FIELD_NAME] = FinishedField::getStorageDefinition($entity_type->id());
    $definitions[ImageField::FIELD_NAME] = ImageField::getStorageDefinition($entity_type->id());
    $definitions[MaxMinutesChainLockedField::FIELD_NAME] = MaxMinutesChainLockedField::getStorageDefinition($entity_type->id());
    $definitions[MaxOpenChainsField::FIELD_NAME] = MaxOpenChainsField::getStorageDefinition($entity_type->id());
    $definitions[MaxOpenChainsPerUserField::FIELD_NAME] = MaxOpenChainsPerUserField::getStorageDefinition($entity_type->id());
    $definitions[MinChainLinksBetweenContributionsField::FIELD_NAME] = MinChainLinksBetweenContributionsField::getStorageDefinition($entity_type->id());
    $definitions[MinChainLinksToPublishField::FIELD_NAME] = MinChainLinksToPublishField::getStorageDefinition($entity_type->id());
    $definitions[RoomsField::FIELD_NAME] = RoomsField::getStorageDefinition($entity_type->id());
    return $definitions;
  }

  /**
   * Implements hook_entity_bundle_info_alter().
   *
   * Set bundle classes for each of the diboo content types.
   *
   * @param array $bundles
   *   An array of bundles, keyed first by entity type, then by bundle name.
   */
  #[Hook('entity_bundle_info_alter')]
  public function entityBundleInfoAlter(&$bundles): void {
    if (!isset($bundles['node'])) {
      return;
    }
    if (isset($bundles['node']['diboo_room'])) {
      $bundles['node']['diboo_room']['class'] = Room::class;
    }
    if (isset($bundles['node']['diboo_chain'])) {
      $bundles['node']['diboo_chain']['class'] = Chain::class;
    }
    if (isset($bundles['node']['diboo_phrase'])) {
      $bundles['node']['diboo_phrase']['class'] = Phrase::class;
    }
    if (isset($bundles['node']['diboo_image'])) {
      $bundles['node']['diboo_image']['class'] = Image::class;
    }
  }

  /**
   * Implements hook_entity_view_display_alter().
   *
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The entity view display used to display the entity components.
   * @param array $context
   *   An associative array containing:
   *     entity_type: The entity type, e.g., 'node' or 'user'.
   *     bundle: The bundle, e.g., 'page' or 'article'.
   *     view_mode: The view mode, e.g., 'full', 'teaser', etc.
   */
  #[Hook('entity_view_display_alter')]
  public function entityViewDisplayAlter(EntityViewDisplayInterface $display, array $context): void {
    if ($context['entity_type'] === 'node' && $context['bundle'] === 'diboo_room') {
      Room::configureViewModeFull($display);
    }
    if ($context['entity_type'] === 'node' && $context['bundle'] === 'diboo_chain') {
      Chain::configureViewModeFull($display);
    }

    // Configure chain links when the module manage_display is enabled.
    if (
      $context['entity_type'] === 'node' &&
      $context['view_mode'] === 'diboo_chain_link' &&
      // Has uid, as a component or hidden.
      ($display->getComponent('uid') || in_array('uid', array_keys($display->get('hidden')), TRUE))
    ) {
      $author = [];
      $author['label'] = 'hidden';
      $author['type'] = 'entity_reference_label';
      // @todo Enable linking when there is user profiles.
      $author['settings'] = ['link' => FALSE];
      $display->setComponent('uid', $author);

      $title = $display->getComponent('title');
      $title['settings']['link_to_entity'] = FALSE;
      $display->setComponent('title', $title);
    }
    // Configure image for chain links.
    if ($context['entity_type'] === 'node' && $context['view_mode'] === 'diboo_chain_link' && $image = $display->getComponent('diboo_image')) {
      $image['label'] = 'hidden';
      $display->setComponent('diboo_image', $image);

      // Hide the title in case it appears with a module such as
      // https://www.drupal.org/project/manage_display
      $display->removeComponent('title');
    }
  }

}
