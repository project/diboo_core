<?php

namespace Drupal\diboo_core\Hook;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\diboo_core\Entity\Room;
use Drupal\views\Plugin\ViewsHandlerManager;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Add finished chains to a room page.
 */
#[Hook('node_view')]
class FinishedChains {

  use StringTranslationTrait;

  /**
   * Constructs a FinishedChains object.
   *
   * @param \Drupal\views\Plugin\ViewsHandlerManager $joinHandler
   *   The route matcher.
   */
  public function __construct(
    private ViewsHandlerManager $joinHandler,
  ) {
  }

  /**
   * Implements hook_ENTITY_TYPE_view().
   *
   * Case for node.
   *
   * @param array $build
   *   The render array.
   * @param \Drupal\Core\Entity\EntityInterface $room
   *   The entity.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The display object.
   * @param string $viewMode
   *   The name of the view mode.
   */
  public function __invoke(array &$build, EntityInterface $room, EntityViewDisplayInterface $display, $viewMode): void {
    if (!$room instanceof Room || $viewMode !== 'full') {
      return;
    }

    // @todo Change into a template so themes can personalize it.
    $build['diboo_core_finished_chains_title'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => $this->t('Finished chains'),
      '#weight' => 20,
    ];
    $build['diboo_core_finished_chains'] = [
      '#type' => 'view',
      '#name' => 'diboo_finished_chains',
      '#display_id' => 'embed',
      '#arguments' => [$room->id()],
      '#embed' => TRUE,
      '#weight' => 30,
    ];
  }

  /**
   * Implements hook_views_query_alter().
   *
   * Since `diboo_finished_chains` lists content without filtering
   * a filter needs to be injected from the argument.
   */
  #[Hook('views_query_alter')]
  public function viewsQueryAlter(ViewExecutable $view, QueryPluginBase $query): void {
    if ($view->id() !== 'diboo_finished_chains') {
      return;
    }
    $definition = [
      'type' => 'inner',
      'table' => 'node__diboo_rooms',
      'field' => 'entity_id',
      'left_table' => 'node_field_data',
      'left_field' => 'nid',
    ];
    $join = $this->joinHandler->createInstance('standard', $definition);
    /** @var \Drupal\views\Plugin\views\query\Sql $query */
    $query->addRelationship('node__diboo_rooms', $join, 'node');
    // Add a new condition to filter by room ID.
    $query->where[1]['conditions'][] = [
      'field' => 'node__diboo_rooms.diboo_rooms_target_id',
      'value' => reset($view->args),
      'operator' => '=',
    ];
  }

}
