<?php

namespace Drupal\diboo_core\Hook;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Add the last chain of a link when another link is being added.
 *
 * Form ID: node_diboo_phrase_diboo_chain_link_form.
 */
#[Hook('form_node_form_alter')]
class LastChainLink {

  use StringTranslationTrait;

  /**
   * Constructs a LastChainLink object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route match service.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected RouteMatchInterface $routeMatch,
  ) {
  }

  /**
   * Render the last chain link of the chain given in the route.
   *
   * @param array $form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current state of the form.
   */
  public function __invoke(array &$form, FormStateInterface $formState): void {
    // Alter only forms for new chain links.
    /** @var \Drupal\node\NodeForm $nodeForm */
    $nodeForm = $formState->getFormObject();
    if ($nodeForm->getOperation() !== 'diboo_chain_link') {
      return;
    }

    // All chain links need a chain to be displayed in this mode.
    $chain = $this->routeMatch->getParameter('chain');
    if (!$chain) {
      return;
    }
    $chainLinks = $chain->get('diboo_chain_links')->referencedEntities();
    $lastChainLink = end($chainLinks);

    // Only phrases will display the title field.
    if ($lastChainLink->bundle() === 'diboo_image') {
      $lastChainLink->set('title', '');
    }

    $viewBuilder = $this->entityTypeManager->getViewBuilder('node');
    $render = $viewBuilder->view($lastChainLink, 'diboo_chain_link');
    $render['#theme'] = 'node__diboo_chain_link';
    $render['#weight'] = -10;
    $form[] = $render;
  }

}
