<?php

namespace Drupal\diboo_core\Hook;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\diboo_core\Entity\Room;

/**
 * Add the open chains to the render array of a room.
 */
#[Hook('node_view')]
class OpenChains {

  use StringTranslationTrait;

  /**
   * Constructs a OpenChains object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   */
  public function __construct(
    private EntityTypeManagerInterface $entityTypeManager,
    private DateFormatterInterface $dateFormatter,
    private AccountProxyInterface $currentUser,
  ) {
  }

  /**
   * Implements hook_ENTITY_TYPE_view().
   *
   * Case for node.
   *
   * @param array $build
   *   The render array.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The display object.
   * @param string $viewMode
   *   The name of the view mode.
   */
  public function __invoke(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $viewMode): void {
    if (!$entity instanceof Room || $viewMode !== 'full') {
      return;
    }

    $build['diboo_core_open_chains'] = [
      '#theme' => 'field',
      '#title' => $this->t('Open chains'),
      '#label_display' => 'above',
      // Prevent quickedit from editing this field by using a special view
      // mode.
      // @see quickedit_preprocess_field()
      '#view_mode' => '_custom',
      '#language' => $entity->language()->getId(),
      '#field_name' => 'diboo_core_open_chains',
      '#field_type' => 'diboo_core_open_chains',
      '#field_translatable' => FALSE,
      '#entity_type' => $entity->getEntityTypeId(),
      '#bundle' => $entity->bundle(),
      '#object' => $entity,
      '#formatter' => 'diboo_core_open_chains',
      '#is_multiple' => TRUE,
      // Without #children the field will not show up.
      '#children' => '',
      '#cache' => [
        'contexts' => ['user'],
        // @todo Define a better tag with bundles that are chains like node_list:BUNDLE.
        'tags' => ['node_list'],
      ],
      '#weight' => 10,
    ];

    foreach ($this->viewElements($entity) as $key => $element) {
      // Only keys in "#items" property are required in
      // template_preprocess_field().
      $build['diboo_core_open_chains']['#items'][$key] = new \stdClass();
      $build['diboo_core_open_chains'][$key] = $element;
    }
  }

  /**
   * Build render array for each open chain.
   *
   * @return array
   *   Elements for render.
   */
  public function viewElements(Room $room): array {
    $elements = [];
    $openChains = $room->getOpenChains();
    $viewBuilder = $this->entityTypeManager->getViewBuilder('node');
    if (!$this->currentUser->hasPermission('add chain links to chains')) {
      $elements[] = [
        '#markup' => $this->t('<a href="@register">Register</a> or <a href="@login">log in</a> to participate', [
          '@register' => Url::fromRoute('user.register')->toString(),
          '@login' => Url::fromRoute('user.login')->toString(),
        ]),
      ];
      return $elements;
    }

    if (!$openChains) {
      $elements[] = [
        '#markup' => $this->t('No chains open'),
      ];
      return $elements;
    }

    /** @var \Drupal\diboo_core\Entity\Chain $chain */
    foreach ($openChains as $chain) {
      $render = $viewBuilder->view($chain, 'diboo_open_chain');
      $render['#theme'] = 'node__diboo_open_chain';

      $chainLinks = $chain->get('diboo_chain_links')->referencedEntities();
      $lastChainLink = end($chainLinks);
      $lastChainLinkType = $lastChainLink->get('type')->first()->get('entity')->getValue();

      $render['chain_links_count'] = [
        '#markup' => count($chainLinks),
      ];
      $render['last_chain_link_type'] = [
        '#markup' => strtolower($lastChainLinkType->label()),
      ];
      $render['last_author'] = [
        '#markup' => $lastChainLink->getOwner()->getDisplayName(),
      ];
      $render['last_chain_link_date'] = [
        '#markup' => $this->dateFormatter->format($lastChainLink->getCreatedTime(), 'short'),
      ];

      // @todo check for anonymous as current contributor.
      $currentAuthorId = $chain->get('diboo_current_contributors')->first()?->get('target_id')->getValue();
      if ($currentAuthorId !== NULL) {
        /** @var \Drupal\user\Entity\User $currentContributor */
        $currentContributor = $this->entityTypeManager->getStorage('user')->load($currentAuthorId);
        $render['current_contributor'] = ['#markup' => $currentContributor->getDisplayName()];
      }

      $availableChainLinkTypes = $this->getAvailableChainLinkTypes($lastChainLink->bundle());
      $links = [];
      foreach ($availableChainLinkTypes as $chainLinkType) {
        $url = Url::fromRoute(
          'diboo_core.add_chain_link',
          [
            'node_type' => $chainLinkType->id(),
            'chain' => $chain->id(),
          ],
          [
            'query' => ['destination' => $room->toUrl()->toString()],
          ]
        );
        // @todo Retrieve reasons for the access denied and display them.
        if (!$url->access()) {
          continue;
        }
        $isImage = $chainLinkType->id() === 'diboo_image';
        $links['add_chain_link_' . $chainLinkType->id()] = [
          'title' => $isImage ? $this->t('Draw') : $this->t('Describe'),
          'url' => $url,
        ];
      }

      $elements[] = [
        '#type' => 'details',
        '#title' => $this->t('Chain started by @author on @date', [
          '@author' => $chain->getOwner()->getDisplayName(),
          '@date' => $this->dateFormatter->format($chain->getCreatedTime(), 'short'),
        ]),
        'details' => $render,
      ];
      $elements[] = [
        '#type' => 'operations',
        '#links' => $links,
      ];
    }
    return $elements;
  }

  /**
   * Gets a list of available chain link types.
   *
   * @param string $chainLinkTypeToExclude
   *   Remove from the set.
   *
   * @return \Drupal\node\NodeTypeInterface[]
   *   Node types.
   */
  protected function getAvailableChainLinkTypes(string $chainLinkTypeToExclude = ''): array {
    $nodeTypes = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    return array_filter($nodeTypes, function ($nodeType) use ($chainLinkTypeToExclude) {
      if ($nodeType->id() === $chainLinkTypeToExclude) {
        return FALSE;
      }
      // @todo Add a generic way to retrieve all possible chain link types.
      if ($nodeType->id() === 'diboo_phrase' || $nodeType->id() === 'diboo_image') {
        return TRUE;
      }
      return FALSE;
    });
  }

}
