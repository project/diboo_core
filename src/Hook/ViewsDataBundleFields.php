<?php

namespace Drupal\diboo_core\Hook;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\diboo_core\EntityViewsData;

/**
 * Add field data for views.
 *
 * Remove when Drupal core add this by itself.
 *
 * @see https://www.drupal.org/node/2898635
 */
#[Hook('views_data')]
class ViewsDataBundleFields {

  /**
   * Constructs a ViewsDataBundleFields object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   */
  public function __construct(
    private EntityTypeManagerInterface $entityTypeManager,
    private ModuleHandlerInterface $moduleHandler,
    private TranslationInterface $stringTranslation,
    private EntityFieldManagerInterface $entityFieldManager,
  ) {
  }

  /**
   * Implements hook_views_data().
   */
  public function __invoke(): array {
    $entityType = $this->entityTypeManager->getDefinition('node');
    $storage = $this->entityTypeManager->getStorage('node');
    return (new EntityViewsData(
      $entityType,
      $storage,
      $this->entityTypeManager,
      $this->moduleHandler,
      $this->stringTranslation,
      $this->entityFieldManager,
    ))->getViewsData();
  }

}
